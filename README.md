# Node.js + Express.js + MongoDB +  React.js

(Frontend) = A simple patient monitoring schedule app using Node.js and React.js

(Backend) = This is a user login and registration app using Node.js, Express, Passport, Mongoose, EJS, and some other packages.

### Quick Start

# Install dependencies for server
npm install

# Install dependencies for client(frontend)
npm run client-install

# Run the backend & frontend with concurrently
npm run dev

# Run the Express backend only
npm run server

# Run the React frontend only
npm run client


# Backend runs on http://localhost:4000 and Frontend on http://localhost:3000
```

### MongoDB

Open "config/keys.js" and add your MongoDB URI (Atlas) - by default using my account
