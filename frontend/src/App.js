import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { items: [], values: {}, errors: {} };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  render() {
    return (
      <div id="App">
        <form onSubmit={this.handleSubmit}>
          <h5>Select Patient Self-Monitoring Schedule*</h5>
          <select className="app-select" name="time" value={this.state.values.time} onChange={this.handleChange}>
            <option value="">By Time Slot</option>
            <option value="7:30"> 7:30 </option>
            <option value="8:12"> 8:12 </option>
            <option value="3:45"> 3:45 </option>
            <option value="5:20"> 5:20 </option>
            <option value="abcdABCD"> abcde (testing the validation) </option>
            <option value="000"> 0 (testing the validation) </option>
          </select>
          <select className="app-select" name="fre" value={this.state.values.fre} onChange={this.handleChange}>
            <option value="">By Frequency</option>
            <option value="5"> 5 </option>
            <option value="10"> 10 </option>
            <option value="15"> 15 </option>
            <option value="16"> 16 </option>
            <option value="abcdABCD"> abcde (testing the validation) </option>
            <option value="000"> 0 (testing the validation) </option>
          </select>
          <div className="error"> 
            <ul>
              <li><span>{this.state.errors.time}</span></li>
              <li><span>{this.state.errors.fre}</span></li>
            </ul>
          </div>
          <div><button className="app-btn">Save</button></div>
        </form>
        <TodoList items={this.state.items} />
      </div>
    );
  }

  handleChange(e) {
    //this.setState({ values: e.target.value });
    let values = this.state.values;
      values[e.target.name] = e.target.value;
      this.setState({
        values
      });
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.validateForm()) {
      let values = {};
          values["time"] = "";
          values["fre"] = "";
          this.setState({values:values});
    }
    else if (!this.state.values.length) {
      return;
    }
      const newItem = {
        values1: this.state.values.time,
        values2: this.state.values.fre,
        jumlah: this.state.items.length + 1,
        id: Date.now()
      };
      this.setState(state => ({
        items: state.items.concat(newItem),
      }));
    
  }

  validateForm() {

    let values = this.state.values;
    let errors = {};
    let formIsValid = true;

    if (!values["time"]) {
      formIsValid = false;
      errors["time"] = "*The Time Slot value is required.";
    } else if (!values["time"].match(/^.*(?=.{1,})(?=.*[0-9]).*$/)) {
        formIsValid = false;
        errors["time"] = "*The Time slot value must be Number.";
    } else if (values["time"].match(/^[0]{1,}$/)) {
      formIsValid = false;
      errors["time"] = "*The Time slot value should be more than 0 (>0).";
    } 

    if (!values["fre"]) {
      formIsValid = false;
      errors["fre"] = "*The Frequency value is required.";
    } else if (!values["fre"].match(/^.*(?=.{1,})(?=.*[0-9]).*$/)) {
        formIsValid = false;
        errors["fre"] = "*The Frequency value must be Number.";
    } else if (values["fre"].match(/^[0]{1,}$/)) {
      formIsValid = false;
      errors["fre"] = "*The Frequency value should be more than 0 (>0).";
    } 

    this.setState({
      errors: errors
    });
    return formIsValid;

  }

}

class TodoList extends Component {
  render() {
    return (
      <ul>
        {this.props.items.map(item => (
          <li key={item.id}>{item.values1} - {item.values2}</li>
        ))}
      </ul>
    );
  }
}

export default App;
